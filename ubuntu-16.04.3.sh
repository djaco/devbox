#!/bin/bash
#
# Run this script:
# wget -O - https://php3.it/bootstrap.sh | bash


set -x

# ##################################################
# Install:
#
#   $ curl http://paolo.php3.it/paolo/provisioning/ubuntu-16.04.3.sh | bash
#
# ##################################################

# ##################################################
# root user
sudo su -
echo "User: root"

## OS updates
apt-get update
apt-get upgrade -y
apt-get dist-upgrade

## Packages
### cli tools
apt-get install -y curl git htop mc terminator tree vim

### deps
# backup sources.list
cp /etc/apt/sources.list /tmp/
# abilito repo src
sed -i '/^#\sdeb-src /s/^# //' "/etc/apt/sources.list"

# dipendenze python 3
apt-get update && apt-get build-dep -y python3.5

# restore sources.list
cp /tmp/sources.list /etc/apt/sources.list

### development tools (gui)
# vscode
# https://code.visualstudio.com/docs/setup/linux
curl https://packages.microsoft.com/keys/microsoft.asc > /tmp/microsoft.asc
cat /tmp/microsoft.asc | gpg --dearmor > /etc/apt/trusted.gpg.d/microsoft.pgp
echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EB3E94ADBE1229CF
apt-get update
apt-get install -y code

### project packages
apt-get install -y docker

### Binaries
curl -fLo \
    /usr/local/bin/yadm \
    https://github.com/TheLocehiliosan/yadm/raw/master/yadm
chmod a+x /usr/local/bin/yadm

# ##################################################
# local user
DEFAULT_USER=paolo

sudo -H -u $DEFAULT_USER bash <<EOF
# set -x
cd ~/
echo "Switched to unprivileged user... $DEFAULT_USER"

# bash-it
git clone --depth=1 https://github.com/Bash-it/bash-it.git ~/.bash_it
~/.bash_it/install.sh --silent
. ~/.bashrc

## pyenv
git clone https://github.com/pyenv/pyenv.git ~/.pyenv

### pyenv->python 3.6 latest
echo "pyenv install Python v3.6.2"
~/.pyenv/bin/pyenv install 3.6.2
echo "3.6.2" > ~/.python-version

echo "enable bash-it pyenv plugin"
bash-it enable plugin pyenv
# BASH_IT_AUTOMATIC_RELOAD_AFTER_CONFIG_CHANGE=1 bash-it enable plugin pyenv

# vim plugins
# TODO
# sh -c "$(curl -fsSL https://raw.githubusercontent.com/liuchengxu/space-vim/master/install.sh)"
EOF

# reboot

